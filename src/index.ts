import './style.css'

import { Game } from './com/Game';

let game

window.addEventListener('DOMContentLoaded', () => {
  console.log("Loading...");
  game = new Game('app')
})

window.addEventListener("resize", function () { // Watch for browser/canvas resize events
  game.engine.resize();
});





