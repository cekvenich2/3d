
import {
  Scene,
  Engine,
  FreeCamera,
  Vector3,
  MeshBuilder,
  CubeTexture,
  Texture,
  PBRMaterial,
  SceneLoader,
} from "@babylonjs/core";
import "@babylonjs/loaders";
import {  HemisphericLight
} from "@babylonjs/core/Lights/hemisphericLight";
import {  Mesh
} from "@babylonjs/core/Meshes/mesh";

import {
  GridMaterial
} from "@babylonjs/materials/grid";

// Required side effects to populate the Create methods on the mesh class. Without this, the bundle would be smaller but the createXXX methods from mesh would not be accessible.
import "@babylonjs/core/Meshes/meshBuilder";

import "@babylonjs/core/Materials/Textures/Loaders"

import '@babylonjs/loaders/glTF'

// Without this next import, an error message like this occurs loading controller models:
//  Build of NodeMaterial failed" error when loading controller model
//  Uncaught (in promise) Build of NodeMaterial failed: input rgba from block
//  FragmentOutput[FragmentOutputBlock] is not connected and is not optional.
import '@babylonjs/core/Materials/Node/Blocks'

// Import animatable side-effects with recent babylon v5.0.x releases for 
// loading controllers, else:
//  "TypeError: sceneToRenderTo.beginAnimation is not a function
//   at WebXRMotionControllerTeleportation2._createDefaultTargetMesh (WebXRControllerTeleportation.ts:751:29)"
import '@babylonjs/core/Animations/animatable'

import gsap from "gsap";

// /////////////////////////////////////////////////////////////////////////////////

var ably = new Ably.Realtime('j4ilcw.3Wvb1Q:n5lpB-BciXpx902H');
var channel = ably.channels.get('test');

export class Game {

get engine() {
  return this.engine
}

createScene() {
  // This creates and positions a free camera (non-mesh)
  var camera = new FreeCamera("camera1", new Vector3(0, 5, -10), this.scene);
  // This targets the camera to scene origin
  camera.setTarget(Vector3.Zero())
  // This attaches the camera to the canvas
  camera.attachControl(this.canvas, true);

  // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
  const light1 = new HemisphericLight("light1", new Vector3(0, 1, 0), this.scene);

  // Default intensity is 1. Let's dim the light a small amount
  light1.intensity = 0.8;

  const ground = Mesh.CreateGround("ground1", 6, 6, 2, this.scene);
  
  return Game.scene;
}

// Create a canvas element for rendering
canvas
engine
static scene 

constructor(id_) {
  this.canvas = document.getElementById(id_)
  this.engine = new Engine(this.canvas, true)
  Game.scene = new Scene(this.engine)

  const sc = this.createScene()     
  // run the main render loop
  this.engine.runRenderLoop(() => {
    sc.render();
  });

  this.createCharacter()
  this.createChair()
}//()


async createCharacter(): Promise<void> {
  const { meshes, animationGroups } = await SceneLoader.ImportMeshAsync(
    "",
    "./",
    "char.glb"
  );

  meshes[0].rotate(Vector3.Up(), Math.PI);

  console.log("animation groups", animationGroups);

  animationGroups[0].stop();

  animationGroups[2].play(true);
}

async createChair(): Promise<void> {
  const { meshes } = await SceneLoader.ImportMeshAsync(
    "",
    "./chairs/",
    "chair101.glb"
  );

  const c101 = Game.scene.getNodeByName('GreenChair_01')
  c101.rotate(BABYLON.Axis.Y, 3, BABYLON.Space.WORLD);
  gsap.to(c101.position,{x:-2, duration:2})

  // Subscribe to messages on channel
  channel.subscribe('3d', function(message) {
    console.log(message.data)
    gsap.to(c101.position, {x:(-1*message.data), duration:.4})
  })

}
}// class


void Promise.all([
   import('@babylonjs/core/Legacy/legacy'),
   import('@babylonjs/core/Debug/debugLayer'),
   import('@babylonjs/inspector'),
 ]).then(() =>

  Game.scene.debugLayer.show({
    handleResize: true,
    embedMode: true,
    overlay: true,
  })
)

// env
console.log(  import.meta.env.VITE_ABLY_KEY  )



